FROM node:latest
COPY . /moaweb
WORKDIR /moaweb
EXPOSE 4200
RUN npm i
CMD ["npm","start"]