import { Injectable } from "@angular/core";
interface task {
  taskID: string;
  command: string;
  startTime: string;
  lastUpdate: string;
  status: any;
  report: any;
  onDisplay: boolean;
}
interface dataset {
  Name: string;
  MD5: string;
  CreationTime: string;
  UploadedBy: string;
  Size: string;
}

@Injectable()
export class global {
  static userID: string = null;
  static email: string = null;
  static tasks: task[] = null;
  static tasksToDisplay: string[] = [];
  static currentTaskID: string = null;
  static comparisonTaskID: string = null;
  static dataPoints: any[] = null;
  static dataPointsList: any[][] = null;
  static attributesDisplaying: string[] = null;
  static message = "Welcome to {MOA}!";
  static frequency: number = 0;
  static significance: number = 0;
  static axisX = "classified instances";
  static axisY = "classifications correct (percent)";
  static serverAddress = "http://localhost:2025/";
  static datasets: dataset[] = null;
  static servers: string[] = ["http://localhost:2025/"];
  static picture: string = null;
}
