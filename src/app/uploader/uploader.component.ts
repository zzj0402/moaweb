import { Component, OnInit } from "@angular/core";
import { UploaderService } from "./uploader.service";
import { global } from "../global";
import { md5 } from "./md5";

interface dataset {
  Name: string;
  MD5: string;
  CreationTime: string;
  UploadedBy: string;
  Size: string;
}

@Component({
  selector: "app-uploader",
  templateUrl: "./uploader.component.html",
  providers: [UploaderService]
})
export class UploaderComponent implements OnInit {
  userID: string = global.userID;
  ngOnInit() {}
  message: string = "Please upload an .arff file!";
  constructor(private uploaderService: UploaderService) {}
  onPicked(input: HTMLInputElement) {
    let file = input.files[0];
    if (file) {
      console.log(file);
      let fileReader = new FileReader();
      let fileMD5: string = null;
      fileReader.readAsText(file);
      fileReader.onload = e => {
        // console.log(fileReader.result);
        fileMD5 = md5(fileReader.result);
        console.log("File MD5: " + fileMD5);
        let fileExtension = file.name.split(".").pop();
        // console.log(fileExtension);
        if (fileExtension != "arff" && fileExtension != "ARFF") {
          this.message = "File error! Please provide a valid arff file!";
          console.error("File extension error!");
        } else {
          this.uploaderService.upload(file, fileMD5).subscribe(msg => {
            input.value = null;
            this.message = msg;
            this.userID = global.userID;
          });
        }
      };
    }
  }
  displayedColumns: string[] = ["Name", "CreationTime", "UploadedBy", "Size"];
  datasets: dataset[] = null;
  getDatasets() {
    // console.log("get task history!");
    if (!global.userID) {
      console.error("No user ID!");
      this.message = "No user ID provided! Please upload dataset first.";
    } else {
      this.uploaderService.getDatasets().subscribe(msg => {
        this.message = msg;
        this.datasets = global.datasets;
        this.ngOnInit();
      });
    }
  }
}
