import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpErrorResponse
} from "@angular/common/http";
import { of } from "rxjs";
import { catchError, last, map, tap } from "rxjs/operators";
import { MessageService } from "../message.service";
import { global } from "../global";
@Injectable({
  providedIn: "root"
})
export class GetService {
  report: string;
  status: string;
  startTime: string;
  command: string;
  lastUpdate: string;
  constructor(private http: HttpClient, private messenger: MessageService) {}
  getReport(taskID: string) {
    if (!taskID) {
      return;
    }
    // console.log(taskID);
    // console.log(global.userID);
    return this.http
      .post(
        global.serverAddress + "report",
        { taskUUID: taskID },
        { observe: "response", reportProgress: true }
      )
      .pipe(
        map(event => this.getEventMessage(event, taskID)),
        tap(message => this.showProgress(message)),
        last(), // return last (completed) message to caller
        catchError(this.handleError(taskID))
      );
  }
  private getEventMessage(event: HttpEvent<any>, taskID: string) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Get request sent for "${taskID}".`;

      case HttpEventType.Response:
        console.log(event.body);
        this.status = event.body.status.code;
        this.report = event.body.report;
        this.command = event.body.command;
        this.startTime = event.body.startTime;
        this.lastUpdate = event.body.lastUpdate;
        // console.log(this.status);
        // console.log(this.report);
        return `"${taskID}" report returned!`;

      default:
        return `Task "${taskID}" surprising upload event: ${event.type}.`;
    }
  }
  private showProgress(message: string) {
    this.messenger.add(message);
  }
  private handleError(taskID: string) {
    const userMessage = `Getting report of ${taskID} failed.`;

    return (error: HttpErrorResponse) => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message =
        error.error instanceof Error
          ? error.error.message
          : `server returned code ${error.status} with body "${error.error}"`;

      this.messenger.add(`${userMessage} ${message}`);

      // Let app keep running but indicate failure.
      return of(userMessage);
    };
  }
}
