import { Component, OnInit } from "@angular/core";
import { global } from "../../global";
import { GetService } from "./get.service";
@Component({
  selector: "app-setting",
  templateUrl: "./setting.component.html",
  styleUrls: ["./setting.component.css"]
})
export class SettingComponent implements OnInit {
  message = "Settings";
  frequency: number = global.frequency;
  servers: string[] = global.servers;
  currentServer: string = global.serverAddress;
  userID: string = global.userID;
  email: string = global.email;
  constructor(private getService: GetService) {}

  ngOnInit() {
    this.getUserID();
  }

  setFrequency(seconds: number) {
    if (seconds > 0) {
      global.frequency = seconds;
      console.log("Frequency " + global.frequency + " seconds set!");
      this.message = "Frequency " + global.frequency + " seconds set!";
    }
  }

  setSignificance(digits: number) {
    if (digits > 0) {
      global.significance = digits;
      console.log("Significance " + global.significance + " digits set!");
      this.message = "Significance " + global.significance + " digits set!";
    }
  }

  setServerAddress(serverIP: string) {
    if (serverIP) {
      global.serverAddress = serverIP;
      this.currentServer = serverIP;
      console.log("Current serverIP: " + global.serverAddress);
      this.message = "Current server address: " + global.serverAddress;
      if (!global.servers.includes(serverIP)) {
        global.servers.push(serverIP);
        console.log("Servers: " + global.servers);
        this.servers = global.servers;
      }
    }
  }
  getUserID() {
    this.getService.getUserID().subscribe((message: string) => {
      this.message = message;
      this.userID = global.userID;
      this.email = global.email;
    });
  }
  setEmail(email: string) {
    global.email = email;
    console.log(global.email);
    this.ngOnInit();
  }
}
